import java.io.IOException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

/**
 * 
 * CS 378 Modern Web Applications -<br>
 * Assignment 2 Spring framework and Unit testing
 * 
 * @author Sangjin Shin<br>
 *         UTEID: ss62273<br>
 *         CSID: sshin96<br>
 *         Email: sangjinshin@outlook.com
 * 
 */

public class QueryServiceImpl implements QueryService {

  private static final String OPEN_STACK_URL = "http://eavesdrop.openstack.org/meetings";

  private int numFiles;

  public QueryServiceImpl() {
    try {
      Jsoup.connect(OPEN_STACK_URL).timeout(10000).execute();
    } catch (IOException e) {
      e.printStackTrace();
    }
    this.numFiles = 0;
  }

  public int getNumFiles() {
    return this.numFiles;
  }

  protected void setNumFiles(int numFiles) {
    this.numFiles = numFiles;
  }

  /**
   * Query the eavesdrop website and get number of files
   * 
   * @return String output as a result of querying the eavesdrop website
   */
  public String getData(String project, String year) {

    String retVal = "";

    project = project.toLowerCase();

    try {
      Document document = Jsoup.connect(OPEN_STACK_URL).get();
      // Get the project
      Elements links = document.getElementsByAttributeValue("href", project + "/");
      if (links != null && !links.isEmpty()) {
        document = Jsoup.connect(links.attr("abs:href")).get();
        // Get the year
        links = document.getElementsByAttributeValue("href", year + "/");
        if (links != null && !links.isEmpty()) {
          document = Jsoup.connect(links.attr("abs:href")).get();
          // Get all the data
          links = document.getElementsByAttributeValueStarting("href", project);
          // Set number of files
          setNumFiles(links.size());
          retVal = "Number of meeting files: " + numFiles + "\n";
        } else { // Invalid year
          retVal = "Invalid year <" + year + "> for project <" + project + ">\n";
        }
      } else if (!year.matches("[1-2][0-9][0-9][0-9]") || Integer.parseInt(year) > 2017) {
        // Project not found and year non-numeric or out of range (1000-2017)
        retVal = "Project with <" + project + "> not found\nInvalid year <" + year
            + "> for project <" + project + ">\n";
      } else { // Project not found
        retVal = "Project with <" + project + "> not found\n";
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

    return retVal;

  }

}
