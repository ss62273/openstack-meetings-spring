import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 
 * CS 378 Modern Web Applications -<br>
 * Assignment 2 Spring framework and Unit testing
 * 
 * @author Sangjin Shin<br>
 *         UTEID: ss62273<br>
 *         CSID: sshin96<br>
 *         Email: sangjinshin@outlook.com
 * 
 */

@Controller
public class OpenStackMeetingsController {

  private QueryService queryServiceImpl;

  public OpenStackMeetingsController() {

  }

  public OpenStackMeetingsController(QueryService queryServiceImpl) {
    this.queryServiceImpl = queryServiceImpl;
  }

  @RequestMapping(value = "/", produces = "text/plain")
  @ResponseBody
  public String root() {
    return "OpenStack meeting statistics web app is accessible at http://localhost:8080/assignment2/openstackmeetings\n";
  }

  @RequestMapping(value = "/openstackmeetings", produces = "text/plain")
  @ResponseBody
  public String welcome() {
    return "Welcome to OpenStack meeting statistics calculation page. Please provide project and year as query parameters.\n";
  }

  @RequestMapping(value = "/openstackmeetings", params = {"project"}, method = RequestMethod.GET,
      produces = "text/plain")
  @ResponseBody
  public String getProject(@RequestParam("project") String project) {
    return "Required parameter <year> missing\n";
  }

  @RequestMapping(value = "/openstackmeetings", params = {"year"}, method = RequestMethod.GET,
      produces = "text/plain")
  @ResponseBody
  public String getYear(@RequestParam("year") String year) {
    return "Required parameter <project> missing\n";
  }

  @RequestMapping(value = "/openstackmeetings", params = {"project", "year"},
      method = RequestMethod.GET, produces = "text/plain")
  @ResponseBody
  public String getParameters(@RequestParam("project") String project,
      @RequestParam("year") String year) {

    // Input validation
    String retVal = "";
    if (project == null || project == "") {
      retVal += "Required parameter <project> missing\n";
    }
    if (year == null || year == "") {
      retVal += "Required parameter <year> missing\n";
    }
    if (retVal.length() > 0) {
      return retVal;
    }

    return queryServiceImpl.getData(project, year);
  }
}
