/**
 * 
 * CS 378 Modern Web Applications -<br>
 * Assignment 2 Spring framework and Unit testing
 * 
 * @author Sangjin Shin<br>
 *         UTEID: ss62273<br>
 *         CSID: sshin96<br>
 *         Email: sangjinshin@outlook.com
 * 
 */

public interface QueryService {

  public int getNumFiles();

  /**
   * @return String output as a result of querying the eavesdrop website
   */
  public String getData(String project, String year);
}
