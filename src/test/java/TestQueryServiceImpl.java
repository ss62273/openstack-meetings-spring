import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 * 
 * CS 378 Modern Web Applications -<br>
 * Assignment 2 Spring framework and Unit testing
 * 
 * @author Sangjin Shin<br>
 *         UTEID: ss62273<br>
 *         CSID: sshin96<br>
 *         Email: sangjinshin@outlook.com
 * 
 */

public class TestQueryServiceImpl {

  QueryServiceImpl queryService = null;

  @Before
  public void setUp() {
    queryService = new QueryServiceImpl();
  }

  @Test
  public void testGetData() {
    final String PROJECT = "solum";
    final String YEAR = "2015";
    String expectedResult = "Number of meeting files: 4\n";
    String actualResult = queryService.getData(PROJECT, YEAR);
    assertEquals(expectedResult, actualResult);
  }

}
