import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

/**
 * 
 * CS 378 Modern Web Applications -<br>
 * Assignment 2 Spring framework and Unit testing
 * 
 * @author Sangjin Shin<br>
 *         UTEID: ss62273<br>
 *         CSID: sshin96<br>
 *         Email: sangjinshin@outlook.com
 * 
 */

public class TestOpenStackMeetingsController {

  @Mock
  QueryService mockService = null;
  OpenStackMeetingsController openStackMeetingsController;

  @Before
  public void setUp() {
    mockService = mock(QueryService.class);
    openStackMeetingsController = new OpenStackMeetingsController(mockService);
  }

  @Test
  public void testMissingProjectParam() {
    when(mockService.getData("", "2015")).thenReturn("Required parameter <project> missing\n");
    String reply = openStackMeetingsController.getYear("2015");
    assertEquals("Required parameter <project> missing\n", reply);
  }

  @Test
  public void testMissingYearParam() {
    when(mockService.getData("solum", "")).thenReturn("Required parameter <year> missing\n");
    String reply = openStackMeetingsController.getProject("solum");
    assertEquals("Required parameter <year> missing\n", reply);
  }

  @Test
  public void testProjectNotFound() {
    when(mockService.getData("eafeiwbf", "2015")).thenReturn("Project with <eafeiwbf> not found\n");
    String reply = openStackMeetingsController.getParameters("eafeiwbf", "2015");
    assertEquals("Project with <eafeiwbf> not found\n", reply);
    verify(mockService, never()).getData("solum", "");
  }

  @Test
  public void testInvalidYear() {
    when(mockService.getData("solum", "2017"))
        .thenReturn("Invalid year <2017> for project <solum>\n");
    String reply = openStackMeetingsController.getParameters("solum", "2017");
    assertEquals("Invalid year <2017> for project <solum>\n", reply);
    verify(mockService).getData("solum", "2017");
  }

  @Test
  public void testBothInvalidParams() {
    when(mockService.getData("qwerty", "twentyseventeen")).thenReturn(
        "Project with <qwerty> not found\nInvalid year <twentyseventeen> for project <qwerty>\n");
    String reply = openStackMeetingsController.getParameters("qwerty", "twentyseventeen");
    assertEquals(
        "Project with <qwerty> not found\nInvalid year <twentyseventeen> for project <qwerty>\n",
        reply);
    verify(mockService).getData("qwerty", "twentyseventeen");
  }

}
